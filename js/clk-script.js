$(document).ready(function()
{
	var time;
	var x;
	var y;
	var canvas = document.getElementById('cvs');
	var context = canvas.getContext('2d');
	var canvasDiameter = 600;
	var strokeWidth = 36;

	//Rotate context 90 degrees counterclockwise to that 2PI is at the 12 o'clock position
	//Some sort of vodoo magic going on here with the translates.....
	//unsure why this works but you need to first translate to positive, rotate, then translate to negative
	context.translate(canvasDiameter / 2, canvasDiameter / 2);
	context.rotate(-Math.PI/2);
	context.translate(-canvasDiameter / 2, -canvasDiameter / 2);

	//Set Time & Update Every Second
	updateTime();
	updateDate();

	//Draw Default Style
	drawBars();

	//Keeps Time and Date updated
	setInterval(updateTime, 100);
	setInterval(updateDate, 1000);

	//Update Default Style
	setInterval(drawBars, 10);

	function drawBars()
	{
		//Resets the context
		context.clearRect(0,0,600,600);

		//Update time
		time = new Date();

		//Seconds
		context.beginPath();
		context.strokeStyle = '#399ecf';
		context.lineWidth = strokeWidth;
		context.arc(canvasDiameter/2, canvasDiameter/2, 282, 0, (Math.PI/30) * (time.getSeconds() + (time.getMilliseconds()/1000)), false);
		context.stroke();
		context.closePath();

		//Seconds Tick Marks
		context.translate(canvasDiameter / 2, canvasDiameter / 2);
		context.lineWidth = .3;
		context.strokeStyle = '#252525';
		for (var i=0; i < 360; i++)
		{
			x = Math.sin(Math.PI/180*i);
			y = Math.cos(Math.PI/180*i);

			context.beginPath();
			context.moveTo(x*264,y*264);
			context.lineTo(x*300,y*300);
			context.stroke();
		}
		context.translate(-canvasDiameter / 2, -canvasDiameter / 2);

		//Minutes
		context.beginPath();
		context.strokeStyle = '#61b1d9';
		context.lineWidth = strokeWidth;
		context.arc(canvasDiameter/2, canvasDiameter/2, 241, 0, (Math.PI/30) * (time.getMinutes() + (time.getSeconds()/60)), false);		
		context.stroke();
		context.closePath();

		//Minutes Tick Marks
		context.translate(canvasDiameter / 2, canvasDiameter / 2);
		context.lineWidth = .3;
		context.strokeStyle = '#252525';
		for (var i=0; i < 60; i++)
		{
			x = Math.sin(Math.PI/30*i);
			y = Math.cos(Math.PI/30*i);

			context.beginPath();
			context.moveTo(x*223,y*223);
			context.lineTo(x*259,y*259);
			context.stroke();
		}
		context.translate(-canvasDiameter / 2, -canvasDiameter / 2);

		//Hours
		context.beginPath();
		context.strokeStyle = '#81c1e1';
		context.lineWidth = strokeWidth;
		context.arc(canvasDiameter/2, canvasDiameter/2, 200, 0, (Math.PI/6) * (((time.getHours() + 12) % 12) + (time.getMinutes()/60) + (time.getSeconds()/3600) ), false);
		context.stroke();
		context.closePath();

		//Hours Tick Marks
		context.translate(canvasDiameter / 2, canvasDiameter / 2);
		context.lineWidth = .3;
		context.strokeStyle = '#252525';
		for (var i=0; i < 12; i++)
		{
			x = Math.sin(Math.PI/6*i);
			y = Math.cos(Math.PI/6*i);

			context.beginPath();
			context.moveTo(x*180,y*180);
			context.lineTo(x*220,y*220);
			context.stroke();
		}
		context.translate(-canvasDiameter / 2, -canvasDiameter / 2);
	}

	function updateTime()
	{
		time = new Date();
		$('#time').text((((time.getHours() + 11) % 12) + 1).toString() + ':' + ('0' + time.getMinutes().toString()).slice(-2));
		//Center Time
		$('#time').css('margin-top','-' + ($('#time').height()/2 - 5) + 'px');
		$('#time').css('margin-left','-' + ($('#time').width()/2 - 8) + 'px'); //8 or 9
	}

	function updateDate()
	{
		//Set Am or PM
		if (time.getHours() < 12)
		{
			$('#daytime').text('AM');	
		}
		else
		{
			$('#daytime').text('PM');	
		}

		//Set Day
		switch(time.getDay())
		{
			case 0:
				$('#day').text('SUN');
				break;
			case 1:
				$('#day').text('MON');	
				break;
			case 2:
				$('#day').text('TUE');
				break;
			case 3:
				$('#day').text('WED');
				break;
			case 4:
				$('#day').text('THU');
				break;
			case 5:
				$('#day').text('FRI');
				break;
			case 6:
				$('#day').text('SAT');
				break;
		}
		
		//Set Date
		$('#date').text(('0' + time.getDate().toString()).slice(-2) + '.' + ('0' + (time.getMonth() + 1).toString()).slice(-2) + '.' + time.getFullYear().toString().substring(2));
	}
});