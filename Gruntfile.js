module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    cssmin:
    {
      css: 
      {
        src: 'css/style.css',
        dest: 'css/style.min.css'
      }
    },
    uglify:
    {
      js: 
      {
        files:
        {
          'js/clk.min.js': ['js/clk-script.js']
        }
      }
    },
    concat: 
    {
      options: 
      {
          separator: '\n'
      },
      js: 
      {
          src: ['js/jquery-2.1.1.min.js',
                'js/clk.min.js'],
          dest: 'js/combined.min.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.registerTask('default', ['cssmin:css', 'uglify:js', 'concat:js']);

};